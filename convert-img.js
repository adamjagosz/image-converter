const im = require('imagemagick');
const fs = require('fs');
const path = require('path');
const glob = require('glob');
const yargs = require('yargs/yargs');
const { hideBin } = require('yargs/helpers');

const argv = yargs(hideBin(process.argv))
    .option('src', {
        type: 'string',
        default: './_src/**/*.{jpg,png}',
        description: 'Input files glob pattern'
    })
    .option('dist', {
        type: 'string',
        default: './_dist/',
        description: 'Destination directory'
    })
    .option('quality', {
        alias: 'q',
        type: 'number',
        default: 70,
        description: 'Compression quality'
    })
    .option('jpeg', {
        alias: 'j',
        type: 'boolean',
        default: false,
        description: 'Whether to force JPEG output format even for PNG input format'
    })
    .option('webp', {
        alias: 'w',
        type: 'boolean',
        default: true,
        description: 'Whether to use webp output format'
    })
    .option('avif', {
        alias: 'a',
        type: 'boolean',
        default: false,
        description: 'Whether to use avif output format'
    })
    .option('avifSpeed', {
        alias: 'A',
        type: 'number',
        default: 5,
        description: 'Conversion speed for AVIF (0-9)'
    })
    .option('sizes', {
        alias: 's',
        type: 'string',
        description: 'Requested widths in px, separated by comma or spaces, e.g. "480,640"'
    })
    .option('min', {
        alias: 'm',
        type: 'number',
        default: 0,
        description: 'Minimal image width in px'
    })
    .option('max', {
        alias: 'M',
        type: 'number',
        description: 'Maximal image width in px'
    })
    .option('black', {
        alias: 'b',
        type: 'boolean',
        default: false,
        description: 'Set black background for png'
    })
    .option('html', {
        alias: 'H',
        type: 'boolean',
        default: false,
        description: 'Generate picture tag HTML markup'
    })
    .option('baseUrl', {
        alias: 'U',
        type: 'string',
        default: '/',
        description: 'Base URL for the picture tag HTML markup'
    })
    .option('jinja2', {
        alias: 'J',
        type: 'boolean',
        default: false,
        description: 'Generate picture macro jinja2 markup'
    })
    .option('class', {
        alias: 'c',
        type: 'string',
        default: 'w-100 h-auto',
        description: 'Classname for img tag in markup printout'
    })
    .option('console', {
        alias: 'C',
        type: 'boolean',
        default: false,
        description: 'Print markup in the terminal instead of file'
    })
    .option('capMediaSize', {
        alias: 'Z',
        type: 'number',
        default: null,
        description: 'Media size (vw) to calculate separate sources for the picture tag to cap fidelity at 2x'
    })
    .argv;

var targetDir = argv.dist;
if (!fs.existsSync(targetDir)){
    fs.mkdirSync(targetDir);
}

const DEFAULT_SIZES = [
    1280,
    1920,
    2560,
    3840,
];
const PREV_SIZES = [
    480,
    640,
];

let requestedSizes = argv.sizes?.split(/[, ]+/g).map(s=>s.trim()).filter(s=>s);


processFiles();


async function processFiles() {
    glob(argv.src, async (err, paths) => {
        
        let analyzedFiles = 0;
        const fileTargets = await Promise.all(paths.map(async filepath => {
            const sourceFilepath = path.resolve(__dirname, filepath);
            const promise = prepareTargets(sourceFilepath);
            promise.then((item) => {
                analyzedFiles++;
                if (process.stdout.clearLine) process.stdout.clearLine();
                process.stdout.write(`\rAnalyzing files: ${ analyzedFiles }/${ paths.length }... ${ item.sourceFilename }`);
            });
            return promise;
        }));

        let convertedFiles = 0;
        await Promise.all(fileTargets.map(async (item) => {
            const promise = convertTargets(item);
            promise.then(() => {
                convertedFiles++;
                if (process.stdout.clearLine) process.stdout.clearLine();
                process.stdout.write(`\rConverting files: ${ convertedFiles }/${ fileTargets.length }... ${ item.sourceFilename }`);
            });
            return promise;
        }));
        if (process.stdout.clearLine) process.stdout.clearLine();
        process.stdout.write(`\rConverting ${ convertedFiles } files...\n`);

        printMarkup(fileTargets);
    });
}
    

async function prepareTargets(sourceFilepath) {   
    return new Promise((resolve, reject) => {
        const extname = path.extname(sourceFilepath);
        const basename = path.basename(sourceFilepath, extname);
        const sourceFilename = basename + extname;

        im.identify(sourceFilepath, function(err, meta) {
            if (err) throw err;

            // Determine target resolutions

            let maxWidth = meta.width;
            let maxHeight = meta.height;
            if (argv.max && argv.max < maxWidth) {
                maxWidth = argv.max;
                maxHeight = Math.round(meta.height / meta.width * argv.max);
            }

            let sizes;
            if (requestedSizes) {
                sizes = requestedSizes;
            } else {
                if (meta.width == PREV_SIZES.slice(-1)[0]) {
                    sizes = [ ...PREV_SIZES ];
                } else {
                    sizes = [ ...DEFAULT_SIZES ];
                }
            }
            sizes = sizes
                .filter((e,i,a) => a.indexOf(e) === i) // unique
                .filter(s => s <= maxWidth)
                .filter(s => s >= argv.min)

            if (sizes.length == 0) {
                sizes = [ maxWidth ];
            }
            
            // Determine target formats

            let formats = [];
            if ((extname == '.jpg' || extname == '.jpeg') || argv.jpeg) {
                formats.unshift('.jpg');
            }
            if (extname == '.png' && !argv.jpeg) {
                formats.unshift('.png');
            }
            if (argv.webp) {
                formats.unshift('.webp');
            }
            if (argv.avif) {
                formats.unshift('.avif');
            }

            let formatTargets = formats.map(format => ({
                format,
                targets: sizes.map(size => {
                    const baseFilepath = path.resolve(__dirname, targetDir + basename + '-' + size);
                    return {
                        targetFilepath: baseFilepath + format,
                        size,
                    };
                }),
            }));
            
            resolve({
                sourceFilepath,
                sourceFilename,
                maxWidth,
                maxHeight,
                basename,
                formatTargets
            });
        });
    });
}
  

async function convertTargets({ sourceFilepath, formatTargets }) {   
    await Promise.all(
        formatTargets.map(({ targets }) => (
            new Promise((resolve, reject) => {
                targets.forEach(({ targetFilepath, size }) => {
                    let args = [
                        '-define',
                        'heic:speed=' + argv.avifSpeed,
                        // (try to) avoid discolored dark grays when converting some of the files exported from PS
                        '-profile',
                        './GIMP built-in sRGB.icc',
                    ];
                    if (argv.black) {
                        args = [ ...args, '-background', 'black', '-layers', 'flatten' ];
                    }
                    
                    im.convert([sourceFilepath, '-resize', size, '-quality', argv.quality, ...args, targetFilepath], 
                    function(err, stdout) {
                        if (err) throw err;
                        resolve({ sourceFilepath, formatTargets });
                    });
                });
            })
        ))
    );
}


function pictureMacro(mediaVariantsAsc) {
    const main = mediaVariantsAsc.find(v => v.mediaMaxWidth == null);
    const mediaFiles = mediaVariantsAsc.filter(v => v.mediaMaxWidth != null);
    const getSizes = (item) => item.formatTargets[0].targets.map(t => t.size).join(',');
    
    const mediaDict = !mediaFiles.length ? '' : 'media_files={\n\t' +
        mediaFiles.map(item => `'(max-width: ${ item.mediaMaxWidth }px)': { 'filename': '${ item.basename }', 'file_sizes': [${ getSizes(item) }] }`).join(`,\n\t`)
        + '\n}';
    return `{{ picture(media_url, '${ main.basename }', v='?1', formats=[${ main.formatTargets.map(t => `"${ t.format.replace('.', '') }"`).join(',') }], file_sizes=[${ getSizes(main) }], width=${ main.maxWidth }, height=${ main.maxHeight }, img_class='${ argv.class }', ${ mediaDict }) }}`
}


function pictureTag(mediaVariantsAsc) {
    const getType = format => `image/${ format.replace('.', '').replace('jpg', 'jpeg') }`;

    let baseUrl = argv.baseUrl || '/';
    baseUrl = (baseUrl + '/').replace(/\/\/$/, '/');
    let sources = [];

    if (argv.capMediaSize != null) {
        let formatTargets = mediaVariantsAsc.flatMap(({ mediaMaxWidth, formatTargets }) => formatTargets.map(item => ({
            ...item,
            mediaMaxWidth,
        })));
        const formatPrecendence = ['.avif', '.webp', '.jpg'];
        const formatBuckets = {};
        formatTargets.sort((a, b) => formatPrecendence.indexOf(b.format) - formatPrecendence.indexOf(a.format));
        formatTargets.forEach(ft => {
            formatBuckets[ft.format] = formatBuckets[ft.format] || [];
            formatBuckets[ft.format].push(ft);
        });

        Object.keys(formatBuckets).forEach(format => {
            const formatTargets = formatBuckets[format];
            let lastMediaMaxWidth = 0;
            let lastBreakpoint = 0;
            let index = 0;
            formatTargets.forEach(({ mediaMaxWidth, format, targets }) => {
                let lastWidth = 0;
                targets.forEach(({ targetFilepath, size }, mediaIndex) => {
                    let mediaBreakpoint = lastMediaMaxWidth + .02;
                    let srcset = `${ baseUrl }${ path.basename(targetFilepath) } ${ size }w`;
                    let maxDensity = 2;
                    let breakpoint = Math.floor(lastWidth / maxDensity / (argv.capMediaSize / 100)) + .02;
                    breakpoint = index > 0 ?
                        mediaIndex > 0 ? Math.max(breakpoint, mediaBreakpoint) : mediaBreakpoint :
                        null;
                    let media = !breakpoint ? '' :
                        ` media="(min-width: ${ breakpoint }px)"`;
                    if (mediaIndex > 0 && breakpoint <= lastBreakpoint) {
                        sources.pop();
                    }
                    if (!mediaMaxWidth || breakpoint < mediaMaxWidth) {
                        sources.push(`<source srcset="${ srcset }" type="${ getType(format) }"${ media }>`);
                    }
                    lastWidth = size;
                    lastBreakpoint = breakpoint;
                    index++;
                });
                lastMediaMaxWidth = mediaMaxWidth;
            });
        });
        sources = sources.reverse();
    } else {
        mediaVariantsAsc.forEach(({ mediaMaxWidth, formatTargets }) => {
            formatTargets.forEach(({ format, targets }) => {
                let media = mediaMaxWidth ? ` media="(max-width: ${ mediaMaxWidth }px)"` : '';
                let srcset = [ ...targets ].reverse().map(({ targetFilepath, size }, i) => `${ baseUrl }${ path.basename(targetFilepath) } ${ size }w`).join(', ');
                sources.push(`<source srcset="${ srcset }" type="${ getType(format) }"${ media }>`);
            });
        });
    }

    const main = mediaVariantsAsc.find(v => v.mediaMaxWidth == null);
    const defaultFilename = path.basename(main.formatTargets.slice(-1)[0].targets[0].targetFilepath);
    return '<picture>\n\t' +
        sources.join('\n\t') +
        `\n\t<img src="${ `${ baseUrl }${ defaultFilename }` }" width="${ main.maxWidth }" height="${ main.maxHeight }" alt="" class="${ argv.class }" />` +
        '\n</picture>';
}


function printMarkup(fileTargets) {
    let pictures = [];

    fileTargets.forEach(item => {
        item.mediaBasename = item.basename.replace(/_max\d+$/, '');
        item.mediaMaxWidth = parseInt(item.basename.match(/_max(\d+)$/)?.[1]) || null;
    });

    while (fileTargets.length){
        let item = fileTargets.splice(0, 1)[0];
        let matchingMediaVariant;
        let mediaVariantsAsc = [ item ];

        do {
            let matchingMediaVariantIndex = fileTargets.findIndex(item1 => item1.mediaBasename == item.mediaBasename);
            if (matchingMediaVariantIndex > -1) {
                matchingMediaVariant = fileTargets.splice(matchingMediaVariantIndex, 1)[0];
                mediaVariantsAsc.push(matchingMediaVariant);
            } else {
                matchingMediaVariant = null;
            }
        } while (matchingMediaVariant);

        mediaVariantsAsc.sort((a, b) => (a.mediaMaxWidth ?? 99999) - (b.mediaMaxWidth ?? 99999));
        pictures.push(mediaVariantsAsc);
    }
    
    if (argv.jinja2) {
        const jinja2 = pictures.map(mediaVariantsAsc => pictureMacro(mediaVariantsAsc)).join('\n\n');
        if (argv.console) {
            console.log(jinja2);
        } else {
            let textFilepath = path.resolve(__dirname, '__markup.jinja2');
            fs.writeFile(textFilepath, jinja2, 'utf-8', () => {
                console.log('Jinja2 markup saved to ' + textFilepath + '.');
            });
        }
    }

    if (argv.html) {
        const html = pictures.map(mediaVariantsAsc => pictureTag(mediaVariantsAsc)).join('\n\n');
        if (argv.console) {
            console.log(html);
        } else {
            let textFilepath = path.resolve(__dirname, '__markup.html');
            fs.writeFile(textFilepath, html, 'utf-8', () => {
                console.log('HTML markup saved to ' + textFilepath + '.');
            });
        }
    }
}
